/* 
Global functions and node modules */
var express      = require('express'),
    http         = require('http'),
    Deferred     = require("promised-io/promise").Deferred,
    ICHEF        = 'http://ichef.bbci.co.uk/images/ic/272x153/',
    utils        = require('./util/util.js'),
    self         = this,
    ENVIRONMENT,
    MAIN_DOMAIN;

/*
Environment Setup*/
if (process.env.BBC_ENVIRONMENT) {
	if (process.env.BBC_ENVIRONMENT === 'live') {
		ENVIRONMENT = 'data.bbc.co.uk';
		MAIN_DOMAIN = 'http://www.thespace.org';
	} else {
		ENVIRONMENT = 'data.' + process.env.BBC_ENVIRONMENT + '.bbc.co.uk';
		MAIN_DOMAIN = 'http://www.' + process.env.BBC_ENVIRONMENT + '.thespace.org';
	}
} else {
	ENVIRONMENT = 'data.test.bbc.co.uk';
	MAIN_DOMAIN = 'http://www.thespace.org';
}


/*
Creating a new expressjs web app */
exports.app = express();


/*
Setting template system folder and type to JADE */
self.app.set('views', './app/views');
self.app.set('view engine', 'jade');
self.app.set('port', process.env.PORT || 3000);


/*
Assets path config */
self.app.use(express.static(__dirname + '/assets'));


/*
Promised base function that requests data to iSite endpoint.
Accepts as parameter a parter name or null for fallback request (3 featured items on thespace.org homepage)
Returns parsed JSON data */
exports.getRequest = function (partner) {
	var deferred = new Deferred(),
	    req,
	    bodyChunks = [],
	    body,
	    parseString,
		options = {
			host: ENVIRONMENT
		};

	/* If we pass a 3rd party name */
	if (partner) {
		options.path = '/isite2-content-reader-thespace/v1/content/file?project=thespace&id=widget' + partner + '&apikey=igmFSCJQ671aEG2aJtMi7QijOGIfagjW&depth=1';
	} else {
		options.path = '/isite2-content-reader-thespace/v1/content/file?project=thespace&contentId=&apikey=igmFSCJQ671aEG2aJtMi7QijOGIfagjW&depth=1&id=Featured';
	}

	req = http.get(options, function(res) {
		res.on('data', function(chunk) {
			bodyChunks.push(chunk);
		}).on('end', function() {
			body = Buffer.concat(bodyChunks);
			/* NodeJS module responsible to parse XML to JSON data */
			parseString = require('xml2js').parseString;
			parseString(body, function (err, result) {
				/* Checking if the response has the structure we expect */
				if (typeof result.result.document[0].form !== 'undefined') {
					if (partner) {
						deferred.resolve(result.result.document[0].form[0]['section-1'][0]);
					} else {
						deferred.resolve(result.result.document[0].form[0].featured[0].homepage[0].hero[0]);
					}
				} else {
					deferred.reject();
				}
			});
		})
	});

	return deferred.promise;
};


/*
This function gets raw JSON data and returns an object ready for the views to render 
Accepts two types - fallback and partner */
exports.processJSONData = function (type, successData) {
	var successObj         = {},
		featureNr          = [],
		arraySize          = 0;

	if (type === 'fallback') {
		arraySize = utils.objectSize(successData) - 1;

		if (arraySize === 1) {
			featureNr = [successData['homepage-featured-1'][0]];
		} else if (arraySize === 2) {
			featureNr = [successData['homepage-featured-1'][0], successData['homepage-featured-2'][0]];
		} else {
			featureNr = [successData['homepage-featured-1'][0], successData['homepage-featured-2'][0],successData['homepage-featured-3'][0]];
		}
	} else {
		featureNr = [successData.feature1[0], successData.feature2[0], successData.feature3[0]];
		arraySize = 3;
	}

	successObj.type        = [];
	successObj.imageSrc    = [];
	successObj.title       = [];
	successObj.description = [];
	successObj.link        = [];

	for (var i = 0; i < arraySize; i++) {

		successObj.type[i] = featureNr[i].result[0].metadata[0].type[0];

		if (successObj.type[i] === 'contributor') {

			successObj.imageSrc[i]    = ICHEF + featureNr[i].result[0].document[0].form[0].contributor[0].promo_image[0] + '.jpg';
			successObj.title[i]       = featureNr[i].result[0].document[0].form[0].contributor[0].name[0] + ' ' + featureNr[i].result[0].document[0].form[0].contributor[0].surname[0];
			successObj.description[i] = utils.removeTags(utils.html_entity_decode(featureNr[i].result[0].document[0].form[0].contributor[0].read_more[0]));
			successObj.link[i]        = MAIN_DOMAIN + '/artist/view/' + featureNr[i].result[0].document[0].form[0].contributor[0].slug[0];

		} else if (successObj.type[i] === 'news') {

			successObj.imageSrc[i]       = ICHEF + featureNr[i].result[0].document[0].form[0].news[0].image[0] + '.jpg';
			successObj.title[i]          = featureNr[i].result[0].document[0].form[0].news[0].title[0];
			successObj.description[i]    = utils.removeTags(utils.html_entity_decode(featureNr[i].result[0].document[0].form[0].news[0].summary[0]));
			successObj.link[i]           = MAIN_DOMAIN + '/news/view/' + featureNr[i].result[0].document[0].form[0].news[0].slug[0];

		} else if (successObj.type[i] === 'artwork') {

			successObj.imageSrc[i]    = ICHEF + featureNr[i].result[0].document[0].form[0].artwork[0].promo[0] + '.jpg';
			successObj.title[i]       = featureNr[i].result[0].document[0].form[0].artwork[0].title[0];
			successObj.description[i] = utils.removeTags(utils.html_entity_decode(featureNr[i].result[0].document[0].form[0].artwork[0].intro[0]));
			successObj.link[i]        = MAIN_DOMAIN + '/artwork/view/' + featureNr[i].result[0].document[0].form[0].artwork[0].slug[0];

		}
	}

	return successObj;
};


/*
Function that makes the request for a specific 3rd party name (eg Wired) and renders its view and widget reference.
If the uniqueID is invalid or not found the fallback version is rendered. */
exports.render3rdPartyPage = function (req, res) {

	self.getRequest(req.params.id).then(function(successData) {
		/* Renders the correct widget variation */
		res.render('widget' + successData.widgetDesign, self.processJSONData('partner', successData));

	},function(data) {
		self.renderFallbackPage(res);
	});
};


/*
Function that makes the request for the fallback version */
exports.renderFallbackPage = function (res) {
	
	self.getRequest().then(function(successData) {
		res.render('fallback', self.processJSONData('fallback', successData));
	});

};


/*
Route without any parameter - calls the fallback function. A unique ID must always be supplied. */
self.app.get('/', function (req, res) {
	self.renderFallbackPage(res);
});


/*
Route with a parameter - calls the 3rdPartyPage function. */
self.app.get('/:id', function (req, res) {
	self.render3rdPartyPage(req, res);
});


/*
Server setup */
exports.server = self.app.listen(self.app.get('port'), function () {
	console.log("Express server listening on port " + self.app.get('port'));
});