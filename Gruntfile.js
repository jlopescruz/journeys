module.exports = function (grunt) {
    'use strict';
    
    grunt.initConfig({
        
        nodemon: {
            dev: {
                script: ['app/server.js']
            }
        },

        jasmine_node: {
            options: {
                forceExit: true,
                match: '.',
                matchall: false,
                extensions: 'js',
                specNameMatcher: 'spec',
                jUnit: {
                    report: false,
                    savePath : "test-automation/test-xml/",
                    useDotNotation: true,
                    consolidate: true
                }
            },
            all: ['spec/']
        },

		watch: {
            node: {
                expand : true,
                files  : ['app/sass/**', 'app/js/**'],
                tasks  : ['jshint', 'sass', 'uglify']
            }
        },

        jshint: {
            options: {
                browser: true,
                curly: true,
                devel: true,
                eqeqeq: true,
                evil: true,
                immed: true,
                regexdash: true,
                asi : true,
                sub: true,
                trailing: true,
                force: true
            },
            dev: {
                src: [
                    'app/*.js',
                    'app/util/*.js',
                    'app/js/*.js',
                    'Gruntfile.js'
                ]
            }
        },

        sass: {
            prod: {
                options: {
                    compass: true, 
                    noCache: true, 
                    style: 'compressed', 
                    sourcemap: 'none'
                },
                files: {
                    'app/assets/css/main.min.css': 'app/sass/main.scss'
                }
            }
        },

        uglify: {
            my_target: {
                files: {
                    'app/assets/js/main.min.js': ['app/js/jquery.js', 'app/js/main.js']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-jasmine-node');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('unit', ['jasmine_node']);
    grunt.registerTask('dev', ['watch']);
    grunt.registerTask('default', ['jshint', 'sass', 'uglify', 'nodemon']);
};