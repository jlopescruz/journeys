var utils = require('../app/util/util.js');

describe('Util module test - app/util/util.js',function() {

	it('Should remove all HTML markup using removeTags function', function() {
		
		expect(utils.removeTags('<p>Hey<p>')).toBe('Hey');
		
		expect(utils.removeTags('<p>Si&acirc;n Thomas is a Welsh actress who has appeared on stage, on TV and in films. She appeared in <em>Harry Potter and the Order of the Phoenix</em> as the character Amelia Bones. She was part of the cast of the musical <em>Spring Awakening</em> in London until the show closed in 2009. In 2012, she appeared as Atorloppe in <em>Merlin</em>. Thomas currently stars in the TV series <em>Monica Dryden</em> and <em>Eurydices</em>.</p>')).toBe('Si&acirc;n Thomas is a Welsh actress who has appeared on stage, on TV and in films. She appeared in Harry Potter and the Order of the Phoenix as the character Amelia Bones. She was part of the cast of the musical Spring Awakening in London until the show closed in 2009. In 2012, she appeared as Atorloppe in Merlin. Thomas currently stars in the TV series Monica Dryden and Eurydices.');
	});

	it('Should convert all HTML entity characters to UTF-8 encoding', function() {
		
		expect(utils.html_entity_decode('&ndash;')).toBe('–');
		expect(utils.html_entity_decode('&amp;')).toBe('&');

	});

	it('Should check if string is a valid JSON', function () {

		expect(utils.isJson('{"menu": {"id": "file","value": "File","popup": {"menuitem": [{"value": "Open", "onclick": "OpenDoc()"},{"value": "Close", "onclick": "CloseDoc()"}]}}}')).toBe(true);
		expect(utils.isJson('{"menu": {"id": "file","value": "File","popup": {"menuitem": [{"value": "Open", "onclick": "OpenDoc()"},{"value": "Close", onclick": "CloseDoc()"}]}}}')).toBe(false);

	});

	it('Should return the right size for an object', function () {
		var myObject = {prop1: false, prop2: true};

		expect(utils.objectSize(myObject)).toBe(2);
	});

});