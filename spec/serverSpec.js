var server  = require('../app/server.js');
jasmine.getEnv().defaultTimeoutInterval = 500;

describe('Server module test - app/server.js',function () {

	beforeEach(function () {
        this.addMatchers({
            toBeArtistOrNewsOrArtwork: function () {
                var actual = this.actual;
                this.message = function () {
                    return "Expected " + actual + " to be either Artist or News or Artwork";
                };
                return actual === 'contributor' || actual === 'news' || actual === 'artwork';
            }
        });
    });

	it('Should get valid widget data from iSite when 3rd party unique ID is valid', function (done) {
		server.getRequest('wired').then(function(data) {
			expect(typeof data).toBe('object');
			expect(typeof data.slug).not.toBe('undefined');
			expect(typeof data.widgetDesign).not.toBe('undefined');
			done();
		});
	});

	it('Should always have 3 items to show', function (done) {
		server.getRequest('wired').then(function(data) {
			expect(typeof data.feature1).not.toBe('undefined');
			expect(typeof data.feature2).not.toBe('undefined');
			expect(typeof data.feature3).not.toBe('undefined');
			done();
		});
	});

	it('Should only have 3 types of content - News, Artist or Artwork', function (done) {
		server.getRequest('wired').then(function(data) {
			expect(data.feature1[0].result[0].metadata[0].type[0]).toBeArtistOrNewsOrArtwork();
			expect(data.feature2[0].result[0].metadata[0].type[0]).toBeArtistOrNewsOrArtwork();
			expect(data.feature3[0].result[0].metadata[0].type[0]).toBeArtistOrNewsOrArtwork();
			done();
		});
	});

	it('Should reject the function promise if the uniqueID is not valid', function (done) {
		var aux = false;
		server.getRequest('wired2').then(function() {
			
		}, function () {
			aux = true;
			expect(aux).toBe(true);
			done();
		});
	});

	it('Should get valid fallback data from iSite when 3rd party unique ID is not valid', function (done) {
		server.getRequest().then(function (data) {
			expect(typeof data).toBe('object');
			expect(typeof data.slug[0]).not.toBe('undefined');
			expect(typeof data['homepage-featured-1'][0]).not.toBe('undefined');
			expect(typeof data['homepage-featured-2'][0]).not.toBe('undefined');
			expect(typeof data['homepage-featured-3'][0]).not.toBe('undefined');
			done();
		});
	});

	it('Should only have 3 types of content for fallback data - News, Artist or Artwork', function (done) {
		server.getRequest().then(function(data) {
			expect(data['homepage-featured-1'][0].result[0].metadata[0].type[0]).toBeArtistOrNewsOrArtwork();
			expect(data['homepage-featured-2'][0].result[0].metadata[0].type[0]).toBeArtistOrNewsOrArtwork();
			expect(data['homepage-featured-3'][0].result[0].metadata[0].type[0]).toBeArtistOrNewsOrArtwork();
			done();
		});
	});
});