# README #

### NOTES ###

This app is responsible to connect to iSite and Ichef and serve content to an iFrame located on a 3rd party website.
The content will be served as HTML.
The app is expecting a valid uniqueID (eg Wired) as a parameter and will provide a fallback version if the ID is not valid / not found.

More info about the task can be found here:  
https://the-space.atlassian.net/browse/SWD-1110  
https://the-space.atlassian.net/browse/SWD-1111




### SET UP ###

1 - Install node modules
```shell
npm install
```

2 - Run server + front-end tasks
```shell
grunt
```

3 - Run JS Unit tests
```shell
grunt unit
```





### TECHONOLOGIES ###

- NODEJS
- HTML5
- CSS3
- CSS
- Grunt
- Jasmine - Unit tests





### NODE EXTERNAL MODULES ###

- EXPRESSJS - Web Framework
- DEFERRED - NodeJS Promises
- NODEMON - Launch Server
- JADE - Templating
- xml2js - XML to JSON parse





### NODEJS APP ARCHITECTURE ###

This is an NodeJS app with the following structure:

![Alt text](https://dl.dropboxusercontent.com/u/1843460/OnwardJourneys-Diagram.jpg)





### WEB APP ###

After a successfull grunt build, the app will be live on:

```shell
http://localhost:8080/wired
```
